/*Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
 *1st Solution*/
WITH highest_revenue AS (
  SELECT s.staff_id, s.first_name, s.last_name, st.store_id, SUM(p.amount) AS revenue_2017,
         ROW_NUMBER() OVER (PARTITION BY st.store_id ORDER BY SUM(p.amount) DESC) AS row_num
  FROM staff s
  JOIN store st ON s.store_id = st.store_id
  JOIN rental r ON s.staff_id = r.staff_id
  JOIN payment p ON r.rental_id = p.rental_id
  WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
  GROUP BY s.staff_id, s.first_name, s.last_name, st.store_id
)
SELECT staff_id, first_name, last_name, store_id, revenue_2017
FROM highest_revenue
WHERE row_num = 1;
/*2nd Solution*/
SELECT staff_id, first_name, last_name, store_id, revenue_2017
FROM (
  SELECT s.staff_id, s.first_name, s.last_name, st.store_id, SUM(p.amount) AS revenue_2017,
         ROW_NUMBER() OVER (PARTITION BY st.store_id ORDER BY SUM(p.amount) DESC) AS row_num
  FROM staff s
  JOIN store st ON s.store_id = st.store_id
  JOIN rental r ON s.staff_id = r.staff_id
  JOIN payment p ON r.rental_id = p.rental_id
  WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
  GROUP BY s.staff_id, s.first_name, s.last_name, st.store_id
) AS highest_revenue
WHERE row_num = 1;

/*Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
*1st Solution*/
SELECT f.title AS movie_title, COUNT(*) AS rental_count, f.rating AS expected_age
FROM film f
JOIN inventory i ON f.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
GROUP BY f.film_id
ORDER BY rental_count DESC
LIMIT 5;
/*2nd Solution*/
SELECT movie_title, rental_count, expected_age
FROM (
  SELECT f.title AS movie_title, COUNT(*) AS rental_count, f.rating AS expected_age, f.film_id
  FROM film f
  JOIN inventory i ON f.film_id = i.film_id
  JOIN rental r ON i.inventory_id = r.inventory_id
  GROUP BY f.film_id
) AS movie_rentals
ORDER BY rental_count DESC
LIMIT 5;

/*Which actors/actresses didn't act for a longer period of time than the others?
*1st Solution*/
SELECT a.actor_id, MAX(f.release_year) AS last_film, a.first_name, a.last_name 
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON fa.film_id = f.film_id 
GROUP BY a.actor_id
ORDER BY last_film;
/*2nd Solution*/
SELECT actor_id, last_film, first_name, last_name
FROM (
  SELECT a.actor_id, MAX(f.release_year) AS last_film, a.first_name, a.last_name
  FROM actor a
  JOIN film_actor fa ON a.actor_id = fa.actor_id
  JOIN film f ON fa.film_id = f.film_id 
  GROUP BY a.actor_id
) AS actor_films
ORDER BY last_film;

